export const SET_CAR_PRICE = 'SET_CAR_PRICE';
export const SET_CAR = 'SET_CAR';

export const setCarPrice = price => ({
  type: SET_CAR_PRICE,
  price
});

export const setCar = car => ({
  type: SET_CAR,
  car
});