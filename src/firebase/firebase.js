import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyCkKqSH81YTmDxSzS1HOQYvBKH02hszlAE",
  authDomain: "insurance-service-8068e.firebaseapp.com",
  databaseURL: "https://insurance-service-8068e.firebaseio.com",
  projectId: "insurance-service-8068e",
  storageBucket: "insurance-service-8068e.appspot.com",
  messagingSenderId: "872077832791",
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const auth = firebase.auth();

export {
  auth,
};