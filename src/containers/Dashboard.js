import React, { Component } from 'react';
import BoxWrapper from "components/BoxWrapper";
import { connect } from "react-redux";
import { setCarPrice, setCar } from 'actions';
import CarForm from "components/CarForm";
import withAuthorization from 'hocs/withAuthorization';
import styled from 'styled-components';
import background from "assets/images/bitmap@2x.png";
import { auth } from '../firebase';

const PageWrapper = styled.div`
  background-image: url(${ background }), linear-gradient(122deg, #317bda, #33c3c8);
  background-size: cover;
  min-height: 100vh;
  display: flex;
  align-items: center;
`;

const BarLogout = styled.div`
  position: fixed;
  height: 50px;
  top: 0;
  display: flex;
  align-items: center;
  background-image: linear-gradient(122deg, #317bda, #33c3c8);
  width: 100%;
  padding: 25px;
`;

const Button = styled.button`
  appearance: none;
  color: #fff;
  background: transparent;
  border: 0;
  cursor: pointer;
`;

class Dashboard extends Component {
  state = {
    step: 0,
    form : [
      { 
        id: "age",
        type: 'number', 
        label: 'Age of the driver',
        value: '',
        hasError: false,
        errorMessage: ""
      },
      { 
        id: "car",
        type: 'select', 
        label: 'Car', 
        options: [{ value: 'audi', label: 'Audi' }, { value: 'bmw', label: 'BMW' }, { value: 'porshe', label: 'Porshe' }],
        value: 'none',
        hasError: false,
        errorMessage: ""
      },
      { 
        id: "price",
        type: 'price', 
        label: 'Purchase Price',
        value: '',
        hasError: false,
        errorMessage: ""
      }
    ],
  }

  logout = () => {
    auth.doSignOut();
  }

  handleFormChange = (value, id) => {
    let { form } = this.state;
    const key = this.findIndexById(id, form);

    form[key].value = value;
    form[key].hasError = false;
    form[key].errorMessage = "";

    this.setState({
      ...form
    });

  }

  handleFormSubmit = (event) => {
    event.preventDefault();
    let { form } = this.state;

    form.map((field) => {
      if (field.id === "price") return this.validadePrice(field);
      if (field.id === "age") return this.validadeAge(field);
      if (field.id === "car") return this.validadeCar(field);
    });

    let formErrors = this.checkFormErrors();

    if (formErrors.length === 0) {
      let priceKey = this.findIndexById("price", this.state.form);
      let carKey = this.findIndexById("car", this.state.form);
      this.props.setCarPrice(this.state.form[priceKey].value);
      this.props.setCar(this.state.form[carKey].value);
      this.props.history.push('/dashboard/quote')
    }
  }

  checkFormErrors = () => {
    return this.state.form.filter((field) => {
      return field.hasError
    })
  }

  findIndexById(id, form) {
    return form.findIndex(field => field.id === id);
  }

  validadePrice = (field) => {
    let { form } = this.state;
    
    if (field.value < 5000) {
      const key = this.findIndexById(field.id, form);
      form[key].hasError = true;
      form[key].errorMessage = "Sorry! The price of the car is too low";
    }

    return this.setState({
      ...form
    });
  }

  validadeCar = (field) => {
    let { form } = this.state;

    if (field.value === "none") {
      const key = this.findIndexById(field.id, form);
      form[key].hasError = true;
      form[key].errorMessage = "Sorry! The car must be selected";
    }

    return this.setState({
      ...form
    });
  }

  validadeRisk = (field) => {
    let { form } = this.state;
    const carKey = this.findIndexById("car", form);
    const fieldCar = form[carKey];
    const key = this.findIndexById(field.id, form);

    if (field.value < 25 && fieldCar.value === "porshe") {
      form[key].hasError = true;
      form[key].errorMessage = "Sorry! We can not accept this particular risk";

      return this.setState({
        ...form
      });
    }

    form[key].hasError = false;
    form[key].errorMessage = "";

    return form
  }

  validadeAge = (field) => {
    let { form } = this.state;

    if(field.value < 18) {
      const key = this.findIndexById(field.id, form);
      form[key].hasError = true;
      form[key].errorMessage = "Sorry! The driver is too young";

      return this.setState({
        ...form
      });
    }

    form = this.validadeRisk(field);

    return this.setState({
      ...form
    });
  }


  render() {
    const { form } = this.state;

    return (
      <PageWrapper>
        <BarLogout><Button onClick={this.logout}>Logout</Button></BarLogout>
        <BoxWrapper>
          <CarForm
            form={form}
            handleFormSubmit={this.handleFormSubmit}
            handleFormChange={this.handleFormChange} />
        </BoxWrapper>
      </PageWrapper>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setCarPrice: (data) => dispatch(setCarPrice(data)),
    setCar: (data) => dispatch(setCar(data))
  }
}

const authCondition = (authUser) => !!authUser;

export default connect(
  null,
  mapDispatchToProps
)(withAuthorization(authCondition)(Dashboard));