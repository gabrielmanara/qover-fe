import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { auth } from '../firebase';
import styled, { withTheme } from 'styled-components';
import logo from "assets/images/white.svg";
import AuthUserContext from 'hocs/AuthUserContext';

import ButtonCta from "components/ButtonCta";
import TitleH2 from "components/TitleH2";


const PageWrapper = styled.div`
  background-image: linear-gradient(122deg, #317bda, #33c3c8);
  min-height: 100vh;
`;

const Header = styled.div`
  text-align: center;
  margin: 10px;
`;

const Logo = styled.div`
  text-align: center;
  margin-bottom: 30px;
  margin-top: 50px;
`;

const LoginBoxWrapper = styled.div`
  margin: auto;
  max-width: 350px;
`;

const LoginBox = styled.div`
  background: ${props => props.theme.backgroundColors.light};
  box-shadow: 0 2px 12px 3px rgba(52, 43, 43, 0.5);
  border-radius: 5px;
  padding: 20px;
  margin: auto;
  width: 100%;
`;

const LoginForm = styled.div`
  margin-top: 20px;
`;

const FormGroup = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
`;

const FormLabel = styled.label`
  display: block;
  color: ${props => props.theme.textColors.mainColor};
  font-size: 10px;
  margin-bottom: 10px;
`;

const FormInput = styled.input`
  color: #CCC;
  appearance: none;
  height: 35px;
  border-width: 0 0 2px;
  border-color: #317bda;

  &:active,
  &:focus {
    border-color: #172f56;
    outline: 0;
  }
`;

class Login extends Component {
  state = {
    email: '',
    password: '',
    error: ''
  }

  handleInput = (value, key) => {
    this.setState({
      [key]: value
    })
  }


  login = (event) => {
    const {
      email,
      password,
    } = this.state;

    const {
      history,
    } = this.props;

    auth.doSignInWithEmailAndPassword(email, password)
      .then(() => {
        history.push('/dashboard');
      })
      .catch(error => {
        console.log(error)
      });

    event.preventDefault();
  }

  render() {
    const { buttonColors, textColors } = this.props.theme;
    return (
      <PageWrapper>
        <AuthUserContext.Consumer>
          {(context) => context && <Redirect to="/dashboard" />}
        </AuthUserContext.Consumer>

        <LoginBoxWrapper>
        <Logo>
          <img src={logo} alt="logo"/>
        </Logo>

        <LoginBox>
          <Header>
            <TitleH2
              color={textColors.mainColor}
              title={"Welcome at Qover"}
              size={"18px"}
              weight={"300"} />
          </Header>
          
          <LoginForm>
            <FormGroup>
              <FormLabel>E-mail</FormLabel>
              <FormInput 
                onInput={event => this.handleInput(event.target.value, 'email')}>
              </FormInput>
            </FormGroup>

            <FormGroup>
              <FormLabel>Password</FormLabel>
              <FormInput 
                onInput={event => this.handleInput(event.target.value, 'password')}
                type="password">
              </FormInput>
            </FormGroup>
            <ButtonCta
              clickHandler={this.login}
              label={"Sign in to your account"}
              color={"#FFF"}
              size={"full"}
              background={buttonColors.blue}
            />
          </LoginForm>
        </LoginBox>
      </LoginBoxWrapper>
      </PageWrapper>
    )
  }
}

export default withTheme(Login);