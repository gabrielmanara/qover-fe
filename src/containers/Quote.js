import React, { Component } from 'react';
import { connect } from "react-redux";
import { globaOffer, universalOffer } from 'reducers';
import styled, { ThemeProvider } from 'styled-components';
import background from "assets/images/background-travel@3x.png";
import { Toggle } from "components/Toggle";
import TitleH2 from "components/TitleH2";
import ButtonCta from "components/ButtonCta";
import NumberFormat from 'react-number-format';

const themeBlue = {
  background: "#31cfda",
  text: "#fff",
  price: "#fff"
};

const themeWhite = {
  background: "#fff",
  text: "#484848",
  price: "#31cfda"
};

const PageWrapper = styled.div`
  background-image: url(${ background}), linear-gradient(122deg, #F5F5F5, #F5F5F5);
  background-size: 100%;
  background-position: top;
  background-repeat: no-repeat;
  min-height: 100vh;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const ChoosePlan = styled.div`
  display: flex;
  align-items: center;
`;

const PlanOptions = styled.div`
  display: flex;
  width: 90%;
  margin: auto;
  justify-content: space-evenly;
  margin-top: 40px;
  flex-direction: column;

  @media ${props => props.theme.breakpoints.lg} {
    width: 935px;
    flex-direction: row;
  }
`;

const PlanLabel = styled.span`
  font-size: 13px;
  text-transform: uppercase;
  padding: 0 10px;
  font-weight: ${props => props.bold ? "bold" : "normal"};
  color: white;
`;

const PlanCard = styled.div`
  background: ${props => props.theme.background};
  border-radius: 5px;
  overflow: hidden;
  box-shadow: 0px 3px 9px 0px rgba(0, 0, 0, 0.3);
  margin-bottom: 20px;
  
  p {
    color: ${props => props.theme.text};
    margin: 0;
  }

`;

const PlanCardHeader = styled.div`
  font-size: 18px;
  padding: 20px;
  font-weight: bold;
  color: #484848;
  text-align: center;
`;

const PlanCardInfo = styled.div`
  color: ${props => props.theme.price};
  padding: 16px;
  box-shadow: inset 0px 1px 1px 0px rgba(97, 97, 97, 0.33);
  text-align: center;
  background-color: rgba(255, 255, 255, 0.15);
`;

const PlanCardItem = styled.div`
  box-shadow: inset 0px 1px 1px 0px rgba(97, 97, 97, 0.33);
  color: #484848;
  font-size: 12px;
  padding: 20px;
  text-align: center;
  font-weight: bold;

  span {
    font-weight: normal;
  }
`;

const PlanCardDescription = styled.div`
  font-size: 12px;
  margin-top: 8px;
`;

const PlanCardPrice = styled.div`
  display: flex;
  justify-content: center;
`;

const Currency = styled.div`
  font-size: 16px;
  line-height: 1.4;
`;

const Price = styled.div`
  font-size: 38px;
  line-height: 1;
  font-weight: bold;
`;

const PlanCardFooter = styled.div`
  padding: 10px;
`;

class Quote extends Component {
  state = {
    selected: 1,
    monthly: false
  }

  changePlan = (event, id) => {
    this.setState({
      selected: id
    })
  }

  handleToggle = (e) => {
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    this.setState({ [e.target.name]: value });
  }

  render() {
    const renderButton = (id) => {
      if (this.state.selected === id) {
        return <ButtonCta
          clickHandler={event => this.changePlan(event, id)}
          label={"Plan selected"}
          color={"#31cfda"}
          type="bold"
          size={"full"}
          background={"#fff"}
        />
      }

      return <ButtonCta
        clickHandler={event => this.changePlan(event, id)}
        label={"Choose this plan"}
        color={themeWhite.background}
        type="bold"
        size={"full"}
        background={themeWhite.price}
      />
    };

    const renderPrice = (value) => {
      let price = this.state.monthly ? value / 12 : value;
      price = price.toFixed(2);

      return <NumberFormat value={price} displayType={'text'} thousandSeparator={true}/>
    }

    return (
      <PageWrapper>
        <TitleH2 
          color="white"
          title="Select a plan"/>

        <ChoosePlan>
          <PlanLabel>
            Pay Monthly
          </PlanLabel>
          <Toggle name="monthly" checked={this.state.monthly} onChange={this.handleToggle} />
          <PlanLabel bold>
            Pay Yearly
          </PlanLabel>
        </ChoosePlan>
        

        <PlanOptions>

          <ThemeProvider theme={this.state.selected === 1 ? themeBlue : themeWhite}>
            <PlanCard scheme="blue">
              <PlanCardHeader><p>Global</p></PlanCardHeader>
              <PlanCardInfo scheme="blue">
                <PlanCardPrice>
                  <Price>{renderPrice(this.props.globaOffer())}</Price>
                  <Currency>€</Currency>
                </PlanCardPrice>
                <PlanCardDescription>YEARLY incl. taxes</PlanCardDescription>
              </PlanCardInfo>
              <PlanCardItem><p>Maximum duration travel <span>of</span> 90 days</p></PlanCardItem>
              <PlanCardItem><p>Medical expenses reimbursement <span>up to</span> 1.000.000 €</p></PlanCardItem>
              <PlanCardItem><p>Personal assistance abroad <span>up to</span> 5.000 €</p></PlanCardItem>
              <PlanCardItem><p>Travel assistance abroad <span>up to</span> 1.000 € <span>per insured per travel</span></p></PlanCardItem>
              <PlanCardItem><p>Coverage duration: 1 year</p></PlanCardItem>

              <PlanCardFooter>
                {renderButton(1)}
              </PlanCardFooter>
            </PlanCard>
          </ThemeProvider>

          <ThemeProvider theme={this.state.selected === 2 ? themeBlue : themeWhite}>
            <PlanCard scheme="blue">
              <PlanCardHeader><p>Universe</p></PlanCardHeader>
              <PlanCardInfo scheme="blue">
                <PlanCardPrice>
                  <Price>{renderPrice(this.props.universalOffer())}</Price>
                  <Currency>€</Currency>
                </PlanCardPrice>
                <PlanCardDescription>YEARLY incl. taxes</PlanCardDescription>
              </PlanCardInfo>
              <PlanCardItem><p>Maximum duration travel <span>of</span> 180 days</p></PlanCardItem>
              <PlanCardItem><p>Medical expenses reimbursement <span>up to</span> 3.000.000 €</p></PlanCardItem>
              <PlanCardItem><p>Personal assistance abroad <span>up to</span> 10.000 €</p></PlanCardItem>
              <PlanCardItem><p>Travel assistance abroad <span>up to</span> 2.500 € <span>per insured per travel</span></p></PlanCardItem>
              <PlanCardItem><p>Coverage duration: 1 year</p></PlanCardItem>

              <PlanCardFooter>
                {renderButton(2)}
              </PlanCardFooter>
            </PlanCard>
          </ThemeProvider>

        </PlanOptions>

      </PageWrapper>
    )
  }
}

function mapStateToProps(state) {
  return {
    globaOffer: () => globaOffer(state.carQuotation),
    universalOffer: () => universalOffer(state.carQuotation),
  }
}

export default connect(
  mapStateToProps
)(Quote);