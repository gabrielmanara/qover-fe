import React from 'react';
import styled from "styled-components";

const Box = styled.div`
  background: #FFF;
  box-shadow: 0 2px 4px 0 rgba(72, 72, 72, 0.5);
  border-radius: 2px;
  width: 90%;
  margin: auto;
  padding: 60px;
  display: flex;
  justify-content: center;

  @media ${props => props.theme.breakpoints.lg} {
    width: 935px;
  }
`;


const BoxWrapper = (props) => (
  <Box>
    {props.children}
  </Box>
);

export default BoxWrapper;