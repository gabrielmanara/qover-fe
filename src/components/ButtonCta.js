import React from "react";
import styled, { css } from 'styled-components';
import { darken } from "polished";


const Button = styled.button`
  font-size: 14px;
  font-weight: 800;
  width: ${props => props.size === "full" ? "100%" : `${props.size}px`};
  color: ${props => props.color};
  background-color: ${props => props.background};
  appearance: none;
  border: 0;
  height: 45px;
  cursor: pointer;

  ${props => props.type && css`
    height: 50px;
    border-radius: 5px;
  `}


  &:hover,
  &:active {
    background: ${props => darken('0.1', props.background)};
  }
`;

const ButtonCta = ({ color, label, background, size, clickHandler, type }) => (
  <Button 
    onClick={clickHandler}
    color={color}
    size={size}
    type={type}
    background={background}>
    {label}
  </Button>
);

export default ButtonCta;