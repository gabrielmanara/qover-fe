import React, { Component } from 'react';
import styled, { css } from "styled-components";
import NumberFormat from 'react-number-format';

const Group = styled.div`
  display: flex;
  margin-bottom: 25px;
  flex-direction: column;
  align-items: flex-start;

  @media ${props => props.theme.breakpoints.md} {
    flex-direction: row;
    align-items: center;
  }
`;

const Field = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
`;

const FieldSelect = styled.div`
  display: flex;
  width: 100%;
  position: relative;
  flex-direction: column;

  @media ${props => props.theme.breakpoints.md} {
    width: 65%;
  }
`;

const Error = styled.div`
  position: absolute;
  color: red;
  white-space: nowrap;
  bottom: -20px;
  font-size: 14px;
`;


const Label = styled.label`
  font-size: 15px;
  color: #484848;
  margin-bottom: 10px;

  @media ${props => props.theme.breakpoints.md} {
    width: 33%;
    margin-bottom: 0;
  }
`;

const Input = styled.input`
  border: solid 2px rgba(72, 72, 72, 0.2);
  height: 40px;
  padding: 10px;
  border-radius: 2px;
  appearance: none;

  ${props => props.hasError && css`
    border-color: red;
  `}
`;

const InputWrapper = styled.div`
  input {
    border: solid 2px rgba(72, 72, 72, 0.2);
    height: 40px;
    padding: 10px;
    border-radius: 2px;
    appearance: none;
  }
`;

const Select = styled.select`
  border: solid 2px rgba(72, 72, 72, 0.2);
  background: white;
  width: 100%;
  height: 40px;
  padding: 10px;
  border-radius: 2px;
  appearance: none;

  ${props => props.hasError && css`
    border-color: red;
  `}
`;

export default class InputField extends Component {
  render() {
    const { label, 
            type, 
            children, 
            options, 
            handleFormChange, 
            id, 
            value, 
            hasError,
            errorMessage } = this.props;
    
    const selectOptions = (options) => {
      return options.map((option, index) => {
        return <option value={option.value} key={index}>
          {option.label}
        </option>;
      })
    };

    return (
      <div>
        <Group>
          <Label htmlFor="">{label}</Label>

          {type === "number" &&
            <Field>
              <Input
                hasError={hasError}
                value={value}
                onChange={(event) => handleFormChange(event.target.value, id)}
                type="number" />
                <Error>{errorMessage}</Error>
            </Field>
          }

          {type === "price" &&
            <Field>
              <InputWrapper hasError={hasError}>
                <NumberFormat value={value} thousandSeparator={true} onValueChange={(values) => {
                  const { formattedValue, value } = values;
                  // formattedValue = $2,223
                  // value ie, 2223
                  handleFormChange(value, id)
                }} />
              </InputWrapper>

              <Error>{errorMessage}</Error>
            </Field>
          }

          {type === "select" &&
            <FieldSelect>
              <Select
                hasError={hasError}
                value={value}
                allowNegative={false}
                onChange={(event) => handleFormChange(event.target.value, id)}>
                <option value={'none'} disabled></option>
                {selectOptions(options)}
              </Select>
              <Error>{errorMessage}</Error>
            </FieldSelect>
          }
          {children}
        </Group>
      </div>
    )
  }
}
