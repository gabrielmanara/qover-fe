import React from "react";
import styled from 'styled-components';


const Title = styled.h2`
  color: ${props => props.color};
  font-weight: ${props => props.weight};
  font-size: ${props => props.size};
`;

const Titleh2 = ({ color, weight, title, size }) => (
  <Title
    color={color}
    size={size}
    weight={weight}>
    {title}
  </Title>
);

export default Titleh2;