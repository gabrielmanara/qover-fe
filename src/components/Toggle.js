import React from 'react';
import styled from 'styled-components';

const ToggleWrapper = styled.label`
  display: flex;
  position: relative;
  width: 50px;
  height: 30px;

  input {
    display: none;
  }
`;

const Checkbox = styled.input.attrs({
  type: 'checkbox'
})`
  display: none;
`;

const Slider = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  transition: .4s;
  cursor: pointer;
  background-color: #5A95E1;
  border-radius: 40px;

  &:before {
    position: absolute;
    content: "";
    right: 4px;
    top: 4px;
  
    transition: .4s;

    height: 22px;
    width: 22px;
    background-color: white;
    border-radius: 50%;

    ${Checkbox}:checked + & {
      right: auto;
      left: 4px;
    }
  }
`;


export const Toggle = props => {
  return (
    <ToggleWrapper>
      <Checkbox {...props} />
      <Slider />
    </ToggleWrapper>
  );
};