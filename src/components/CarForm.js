import React, { Component } from 'react';
import InputField from "components/InputField";
import styled, { withTheme } from 'styled-components';
import ButtonCta from "components/ButtonCta";

const Form = styled.form`
  display: flex;
  flex-direction: column;

  @media ${props => props.theme.breakpoints.md} {
    width: 60%;
  }
`;

const FormFooter = styled.div`
  width: 100%;
  margin: auto;

  @media ${props => props.theme.breakpoints.md} {
    width: 163px;
  }
`;


class CarForm extends Component {
  render() {
    const { form, handleFormChange, handleFormSubmit } = this.props;
    const { buttonColors } = this.props.theme;
    

    const renderForm = (form) => {
      return form.map((field, index) => {
        return <InputField 
                  handleFormChange={handleFormChange}
                  key={index}
                  id={field.id}
                  value={field.value}
                  hasError={field.hasError}
                  errorMessage={field.errorMessage}
                  type={field.type}
                  options={field.options}
                  label={field.label}
                />
      })
    };

    return (
      <Form onSubmit={handleFormSubmit}>
        {renderForm(form)}
        <FormFooter>
          <ButtonCta
            label={"Get a price"}
            color={"#FFF"}
            size="full"
            type="bold"
            background={buttonColors.lightBlue}
          />
        </FormFooter>
      </Form>
    )
  }
}

export default withTheme(CarForm);
