export const breakpoints = {
  sm: '(min-width: 576px)',
  md: '(min-width: 768px)',
  lg: '(min-width: 992px)',
};

export const backgroundColors = {
  light: "#fff"
}

export const textColors = {
  mainColor: "#5b7289"
}

export const buttonColors = {
  blue: "#317bda",
  lightBlue: "#31cfda"
}