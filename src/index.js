import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import qover from './themes/index';
import { ThemeProvider, injectGlobal } from 'styled-components';
import styledNormalize from 'styled-normalize';
import { createStore } from 'redux'; 
import reducer from './reducers';
import { Provider } from "react-redux";

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

injectGlobal`
  ${styledNormalize}
  @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700');
  
  body {
    font-family: 'Roboto', sans-serif;
  }

  .container {
    width: 100%;
    max-width: 1240px;
    padding: 0 15px 20px;
    margin: auto;
  }

  * {
    box-sizing: border-box;
  }
  *:before,
  *:after {
    box-sizing: border-box;
  }
`

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={qover}>
        <App />
    </ThemeProvider>
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();
