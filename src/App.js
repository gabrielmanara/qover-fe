import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Login from "./containers/Login";
import Dashboard from "./containers/Dashboard";
import Quote from "./containers/Quote";
import withAuthentication from './hocs/withAuthentication';
import styled from 'styled-components';

const AppWrapper = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
`;

class App extends Component {

  render() {

    return (
      <Router>
        <AppWrapper>
          <Route exact path="/" component={Login} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/dashboard/quote" component={Quote} />
        </AppWrapper>
      </Router>
    );
  }
}

export default withAuthentication(App);
