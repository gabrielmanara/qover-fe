import { combineReducers } from "redux";
import {
  SET_CAR_PRICE,
  SET_CAR
} from 'actions/index';

// Avaiable Cars
export const PORSCHE = 'porsche';
export const BMW = 'bmw';
export const AUDI = 'audi';

export const initialState = {
  price: 0,
  car: null,
};


// Reducers
const carQuotation = (state = initialState, action) => {
  const { price, car } = action;

  switch (action.type) {
    case SET_CAR_PRICE:
      return {
        ...state,
        price: price,
      };
    case SET_CAR:
      return {
        ...state,
        car: car
      };
    default:
      return state
  }
}

// Getters
export const globaOffer = state => {
  switch (state.car) {
    case PORSCHE:
      return 500
    case BMW:
      return 150
    case AUDI:
      return 250
    default:
      return 0
  }
}

export const universalOffer = state => {
  switch (state.car) {
    case PORSCHE:
      return 500 + state.price * .0007;
    case BMW:
      return 150 + state.price * .004;
    case AUDI:
      return 250 + state.price * .003;
    default:
      return 0
  }
}

export default combineReducers({
  carQuotation
})